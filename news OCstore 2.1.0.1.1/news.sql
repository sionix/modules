-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 20 2016 г., 10:10
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vipcrystal`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news`
--

CREATE TABLE IF NOT EXISTS `oc_news` (
  `news_id` int(15) NOT NULL AUTO_INCREMENT,
  `date_added` date NOT NULL,
  `date_modified` date NOT NULL,
  `status` int(5) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `oc_news`
--

INSERT INTO `oc_news` (`news_id`, `date_added`, `date_modified`, `status`) VALUES
(6, '2016-01-15', '2016-01-15', 1),
(7, '2016-01-14', '2016-01-14', 1),
(8, '2016-01-15', '2016-01-15', 1),
(9, '2016-01-15', '2016-01-15', 1),
(10, '2016-01-15', '2016-01-15', 1),
(11, '2016-01-15', '2016-01-15', 1),
(12, '2016-01-15', '2016-01-15', 1),
(13, '2016-01-15', '2016-01-15', 1),
(14, '2016-01-15', '2016-01-15', 1),
(15, '2016-01-15', '2016-01-15', 1),
(16, '2016-01-15', '2016-01-15', 1),
(17, '2016-01-15', '2016-01-15', 1),
(18, '2016-01-15', '2016-01-15', 1),
(19, '2016-01-15', '2016-01-15', 1),
(20, '2016-01-15', '2016-01-15', 1),
(21, '2016-01-15', '2016-01-15', 1),
(22, '2016-01-15', '2016-01-15', 1),
(23, '2016-01-15', '2016-01-15', 1),
(24, '2016-01-15', '2016-01-15', 1),
(25, '2016-01-15', '2016-01-15', 1),
(26, '2016-01-15', '2016-01-15', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_news_description`
--

CREATE TABLE IF NOT EXISTS `oc_news_description` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `news_id` int(15) NOT NULL,
  `description` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `language_id` int(15) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Дамп данных таблицы `oc_news_description`
--

INSERT INTO `oc_news_description` (`id`, `news_id`, `description`, `title`, `language_id`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES
(7, 4, '&lt;p&gt;Описание 2&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/p&gt;', 'Новость 2', 1, 'штмл тайтл', 'штмл тег аш1', 'описание', 'ключевые слоова'),
(8, 4, '&lt;p&gt;Description&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/p&gt;', 'NewsItem 2', 2, 'title html', 'html h1', 'description meta', 'meta keywords'),
(9, 3, '&lt;p&gt;Описание Описание Описание &amp;nbsp; &amp;nbsp;dsads&lt;/p&gt;', 'Новость', 1, 'тайтл', 'тег_аш1', 'мета описание', 'ключевые слова'),
(10, 3, '&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;Description&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;Description&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;Description&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;Description&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;Description&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;', 'News Ite', 2, 'title', 'h1', 'meta descr', 'keywiord'),
(11, 5, '&lt;p&gt;DSADASDAS&lt;/p&gt;', 'sDSADSA', 1, '', '', '', ''),
(12, 5, '&lt;p&gt;DSADAS&lt;/p&gt;', 'DASDAS', 2, '', '', '', ''),
(13, 0, '&lt;p&gt;fdafdafdada&lt;/p&gt;', 'fdfdaf', 1, 'fdafda', 'fdafd', 'afdsafdas', 'fdafdsafd'),
(14, 0, '&lt;p&gt;fdsafdsa&lt;/p&gt;', 'fdafdsa', 2, 'fdafdaf', 'dafdsa', 'fdsafdsaf', 'dsafdsa'),
(17, 7, '&lt;p&gt;авфавф&lt;/p&gt;', 'уновость', 1, 'авфавы', 'вафавы', 'авыфавы', 'фавыфавыфа'),
(18, 7, '&lt;p&gt;fdsfdsfd&lt;/p&gt;', 'fdsfsd', 2, 'sfdsfdsf', 'fdsfds', 'fdsfdsfds', 'fdsfds'),
(23, 6, '&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 21.6667px;&quot;&gt;fdafdafdada&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;', 'fdfdaf', 1, 'fdafda', 'fdafd', 'afdsafdas', 'fdafdsafd'),
(24, 6, '&lt;p&gt;fdsafdsa&lt;/p&gt;', 'fdafdsa', 2, 'fdafdaf', 'dafdsa', 'fdsafdsaf', 'dsafdsa'),
(25, 8, '&lt;p&gt;авыфавыфавыфа&lt;/p&gt;', 'авыаывф', 1, 'вфавыфавф', '', '', ''),
(26, 8, '&lt;p&gt;фавыфавыфавыф&lt;/p&gt;', 'авыфавыфавы', 2, '', '', '', ''),
(27, 9, '&lt;p&gt;fdasfdsfdsafd&lt;/p&gt;', 'fdsafdasfdsa', 1, '', '', '', ''),
(28, 9, '&lt;p&gt;fdsafdsafdsafsa&lt;/p&gt;', 'dfsafdsafdsa', 2, '', '', '', ''),
(29, 10, '&lt;p&gt;afdsafdsafsdafdsafd&lt;/p&gt;', 'dafdsafd', 1, 'fdsaf', '', '', ''),
(30, 10, '&lt;p&gt;dfdsfad&lt;/p&gt;', 'fdsaf', 2, '', '', '', ''),
(31, 11, '&lt;p&gt;рпврпавр&lt;/p&gt;', 'рпврпв', 1, '', '', '', ''),
(32, 11, '&lt;p&gt;gfdsgfds&lt;/p&gt;', 'gfsgfs', 2, '', '', '', ''),
(33, 12, '&lt;p&gt;fdsfamjhgjhj&lt;/p&gt;', 'dsfsdf', 1, '', '', '', ''),
(34, 12, '&lt;p&gt;fhfjhf&lt;/p&gt;', 'jhgj', 2, '', '', '', ''),
(35, 13, '&lt;p&gt;jgfjgfhjf&lt;/p&gt;', 'jhfjhgf', 1, '', '', '', ''),
(36, 13, '&lt;p&gt;hfjhgfjhgf&lt;/p&gt;', 'fjhgfjhgfj', 2, '', '', '', ''),
(37, 14, '&lt;p&gt;jfjhgfh&lt;/p&gt;', 'jhfjf', 1, '', '', '', ''),
(38, 14, '&lt;p&gt;hdh&lt;/p&gt;', 'hgfhdd b', 2, '', '', '', ''),
(39, 15, '&lt;p&gt;hdbhdbhd&lt;/p&gt;', 'hdhbd', 1, '', '', '', ''),
(40, 15, '&lt;p&gt;bhdfghdfbh&lt;/p&gt;', 'hbdbfhfd', 2, '', '', '', ''),
(41, 16, '&lt;p&gt;jmkjhmk&lt;/p&gt;', 'kmhk', 1, '', '', '', ''),
(42, 16, '&lt;p&gt;jhmkhjmk&lt;/p&gt;', 'kmjhmk', 2, '', '', '', ''),
(43, 17, '&lt;p&gt;kjhmkjhmkm&lt;/p&gt;', 'mkjhmkjhm', 1, '', '', '', ''),
(44, 17, '&lt;p&gt;mkhjmjhmkj&lt;/p&gt;', 'mkjh', 2, '', '', '', ''),
(45, 18, '&lt;p&gt;vtrwevtwe&lt;/p&gt;', 'tretw', 1, '', '', '', ''),
(46, 18, '&lt;p&gt;rewtrewve&lt;/p&gt;', 'trvwtv', 2, '', '', '', ''),
(47, 19, '&lt;p&gt;rewvtewvtwv&lt;/p&gt;', 'vtrewvt', 1, '', '', '', ''),
(48, 19, '&lt;p&gt;fsvdfsvd&lt;/p&gt;', 'fdsvd', 2, '', '', '', ''),
(49, 20, '&lt;p&gt;vdsvfdsv&lt;/p&gt;', 'fdsvdf', 1, '', '', '', ''),
(50, 20, '&lt;p&gt;avafds&lt;/p&gt;', 'dfsfsaf', 2, '', '', '', ''),
(51, 21, '&lt;p&gt;vfasvfasvd&lt;/p&gt;', 'fvas', 1, '', '', '', ''),
(52, 21, '&lt;p&gt;afvdsa&lt;/p&gt;', 'vfdasv', 2, '', '', '', ''),
(53, 22, '&lt;p&gt;savfas&lt;/p&gt;', 'fvdsaf', 1, '', '', '', ''),
(54, 22, '&lt;p&gt;wgfdgs&lt;/p&gt;', 'gfdg', 2, '', '', '', ''),
(55, 23, '&lt;p&gt;fcdsacfsdc&lt;/p&gt;', 'fcdacfdas', 1, '', '', '', ''),
(56, 23, '&lt;p&gt;cfascfascdfas&lt;/p&gt;', 'fcascfas', 2, '', '', '', ''),
(57, 24, '&lt;p&gt;acfdsacfasdcf&lt;/p&gt;', 'fcdsacfds', 1, '', '', '', ''),
(58, 24, '&lt;p&gt;cfascfsac&lt;/p&gt;', 'fcdsacfsa', 2, '', '', '', ''),
(59, 25, '&lt;p&gt;afasdfcsad&lt;/p&gt;', 'fdafds', 1, '', '', '', ''),
(60, 25, '&lt;p&gt;sacfdsacfsdacf&lt;/p&gt;', 'fdsacfd', 2, '', '', '', ''),
(61, 26, '&lt;p&gt;dacfdacfsdac&lt;/p&gt;', 'fcdascf', 1, '', '', '', ''),
(62, 26, '&lt;p&gt;fdsacfsa&lt;/p&gt;', 'cfadcsa', 2, '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
