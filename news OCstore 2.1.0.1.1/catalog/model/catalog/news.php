<?php
class ModelCatalogNews extends Model {
	public function getNewsItem($news_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE n.news_id = '" . (int)$news_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = '1'");
		return $query->row;
	}

	public function getNews($filter_data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = '1'";
		if ($filter_data['start']) {
			$sql .= " LIMIT " . $filter_data['start'] . ', ' . $filter_data['limit'];


		} // ORDER BY n.sort_order, LCASE(nd.title) ASC
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getNewsCount() {
		$sql = "SELECT COUNT(*) as count FROM " . DB_PREFIX . "news";

		$query = $this->db->query($sql);
		return $query->row['count'];
	}
}