<?php

class ControllerInformationNews extends Controller
{
    public function index()
    {
        $this->load->language('information/news');

        $this->load->model('catalog/news');

        $this->load->model('tool/image');


        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_product_limit');
        }
        $filter_data = array(
            'start'              => ($page - 1) * $limit,
            'limit'              => $limit,
            'page'              => $page
        );

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );


        $news_id = isset($this->request->get['news_id']) ? (int)$this->request->get['news_id'] : false;

        if ($news_id) {
            $this->getNews($news_id);
        } else {
            $this->getList($filter_data);
        }

    }

    public function getNews($news_id)
    {

        $news_info = $this->model_catalog_news->getNewsItem($news_id);

        if ($news_info) {
            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_information'),
                'href' => $this->url->link('information/news')
            );
            if ($news_info['meta_title']) {
                $this->document->setTitle($news_info['meta_title']);
            } else {
                $this->document->setTitle($news_info['title']);
            }

            $this->document->setDescription($news_info['meta_description']);
            $this->document->setKeywords($news_info['meta_keyword']);

            $data['breadcrumbs'][] = array(
                'text' => $news_info['title'],
                'href' => $this->url->link('information/news', 'news_id=' . $news_id)
            );


                $data['heading_title'] = $news_info['title'];
            $data['button_continue'] = $this->language->get('button_continue');

            $data['description'] = html_entity_decode($news_info['description'], ENT_QUOTES, 'UTF-8');

            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['date_added'] = $news_info['date_added'];
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/news_item.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/news_item.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/news_item.tpl', $data));
            }
        } else {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('information/news', 'news_id=' . $news_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }


    }

    public function getList($filter_data)
    {
        $news = $this->model_catalog_news->getNews($filter_data);
        $news_total = $this->model_catalog_news->getNewsCount();
        $data = array();
        $pagination = new Pagination();
        $pagination->total = $news_total;
        $pagination->page = $filter_data['page'];
        $pagination->limit = $filter_data['limit'];
        $pagination->url = $this->url->link('information/news', '&page={page}');
        $this->load->language('information/news');


        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($news_total) ? (($filter_data['page'] - 1) * $filter_data['limit']) + 1 : 0, ((($filter_data['page'] - 1) * $filter_data['limit']) > ($news_total - $filter_data['limit'])) ? $news_total : ((($filter_data['page'] - 1) * $filter_data['limit']) + $filter_data['limit']), $news_total, ceil($news_total / $filter_data['limit']));

        if ($news) {
            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_information'),
                'href' => $this->url->link('information/news')
            );
            $this->document->setTitle($this->language->get('meta_title'));


            $this->document->setDescription($this->language->get('meta_description'));
            $this->document->setKeywords($this->language->get('meta_keyword'));

            $data['button_continue'] = $this->language->get('button_continue');

            foreach ($news as $news_item) {
               $data['news'][] = array(
                'description'  =>  utf8_substr(strip_tags(html_entity_decode($news_item['description'], ENT_QUOTES, 'UTF-8')), 0, 1000) . '..',
                'href'       => $this->url->link('information/news', '&news_id=' . $news_item['news_id']),
                'date_added'        =>  $news_item['date_added'],
                'title' => $news_item['title']
               );
            }
            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/news_list.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/news_list.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/news_list.tpl', $data));
            }
        }

    }
}
