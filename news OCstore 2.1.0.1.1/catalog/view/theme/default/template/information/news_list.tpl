<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <?php foreach($news as $news_item) { ?>
      <div class="col-sm-12 strip">
      <a href="<?=$news_item['href']; ?>"><h1><?php echo $news_item['title']; ?></h1></a>
      <a href="<?=$news_item['href']; ?>"><h4><?php echo $news_item['date_added']; ?></h4></a>
      <?php echo $news_item['description']; ?>
        </div>
      <?php } ?>
      <?php echo $pagination; ?>
      <?php echo $results; ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>