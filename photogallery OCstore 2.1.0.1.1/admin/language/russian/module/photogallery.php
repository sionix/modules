<?php
// Heading
$_['heading_title']    = 'Photogallery';
$_['coded_by']    = 'Coded by Sion';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки модуля обновлены!';
$_['text_edit']        = 'Редактирование модуля';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_alt']     = 'Описание фото[alt]';
$_['entry_text']     = 'Текст под фото';
$_['entry_title']     = 'Заголовок для фото';
$_['button_remove']     = 'Удалить';
$_['button_change_image']     = 'Изменить фото';
$_['button_view_image']     = 'Посмотреть фото';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';