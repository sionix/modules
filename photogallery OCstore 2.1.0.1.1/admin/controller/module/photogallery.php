<?php //coded by sion
class ControllerModulePhotogallery extends Controller
{
    private $error = array();
//files
// controller/module/photogallery
//model/extension/photogallery
//view/photogallery
//photogallery.css
    public function index()
    {
        $this->load->language('module/photogallery');
        $this->load->model('tool/image');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addScript('view/javascript/sionjs/jquery.form.js');
        $this->document->addStyle('view/stylesheet/photogallery.css');
        $this->load->model('setting/setting');
        $this->load->model('extension/photogallery');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('photogallery', $this->request->post);

            if (isset($this->request->post['images'])) {
                foreach ($this->request->post['images'] as $image) {
                    $this->session->data['success'] = $this->language->get('text_success');
                    $save_data['text'] = isset($image['text']) ? $image['text'] : '';
                    $save_data['alt'] = isset($image['alt']) ? $image['alt'] : '';
                    $save_data['title'] = isset($image['title']) ? $image['title'] : '';
                    $save_data['id'] = $image['id'];
                    $this->model_extension_photogallery->saveImage($save_data);

                }
            }


            if ($_FILES['images']['name']) {
                foreach ($_FILES['images']['name'] as $key => $val) {
                    //upload and stored images
                    $save_data = array();

                    $img = $_FILES['images']['name'][$key];

                    $info = new SplFileInfo($img);
                    $img = mt_rand(1, 9999).time() . '.' . $info->getExtension();

                    if (move_uploaded_file($_FILES['images']['tmp_name'][$key], DIR_IMAGE . 'photogallery/' . $img)) {
                        $this->model_extension_photogallery->saveOnlyImage($img);
                    }
                }
            }
            //$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $images = $this->model_extension_photogallery->getImages();
        $data['images'] = array();
        foreach ($images as $k => $image) {
            $data['images'][$k]['image'] = HTTPS_CATALOG . 'image/photogallery/' . $image['image'];
            $data['images'][$k]['thumb'] = $this->model_tool_image->resize('photogallery/' . $image['image'], 60, 60);
            $data['images'][$k]['text'] = $image['text'];
            $data['images'][$k]['alt'] = $image['alt'];
            $data['images'][$k]['id'] = $image['id'];
            $data['images'][$k]['title'] = $image['title'];
        }
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_alt'] = $this->language->get('entry_alt');
        $data['entry_text'] = $this->language->get('entry_text');
        $data['coded_by'] = $this->language->get('coded_by');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_change_image'] = $this->language->get('button_change_image');
        $data['button_view_image'] = $this->language->get('button_view_image');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/photogallery', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['token'] = $this->session->data['token'];

        $data['action'] = $this->url->link('module/photogallery', 'token=' . $this->session->data['token'], 'SSL');
        $data['action_image'] = $this->url->link('module/photogallery/upload', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['category_status'])) {
            $data['category_status'] = $this->request->post['category_status'];
        } else {
            $data['category_status'] = $this->config->get('category_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/photogallery.tpl', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/photogallery')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function delete()
    {
        if (isset($this->request->get['id'])) {
            $this->load->model('extension/photogallery');
            $this->model_extension_photogallery->deleteImage($this->request->get['id']);
            $json = true;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }


    public function upload()
    {
        if (isset($this->request->post['id'])) {
            if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
                $this->load->model('extension/photogallery');
                $target_file = $_FILES['images']['name'];
                if (move_uploaded_file($_FILES['images']['tmp_name'], DIR_IMAGE . 'photogallery/' . $target_file)) {
                    $save_data['image'] = $target_file;
                    $save_data['id'] = $this->request->post['id'];
                    $this->model_extension_photogallery->saveOnlyImage($save_data);
                    $json['image'] = $this->model_extension_photogallery->getImage($this->request->post['id']);

                }
            }
            //$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }
    }

}