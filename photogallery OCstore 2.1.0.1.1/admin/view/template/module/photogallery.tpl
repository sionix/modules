<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category"
                      class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="category_status" id="input-status" class="form-control">
                                <?php if ($category_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="image_form_submit" value="1"/>
                    <label>Choose Image</label>
                    <input type="file" name="images[]" id="images" multiple>
                    <div class="uploading none">
                        <label>&nbsp;</label>
                        <!-- <img src="uploading.gif" alt="uploading......"/> -->
                    </div>
                <div id="images_preview">
                    <?php if (isset($images)) { ?>
                    <?php foreach ($images as $k => $image) { ?>
                    <div class="form-group" id="<?=$k; ?>">
                        <div class="col-sm-2"><a href="#" data-id="<?=$k; ?>" data-href="<?=$image['image']; ?>"><img
                                        src="<?=$image['thumb']; ?>"
                                        alt="<?=$image['alt']; ?>"></a>
                            <!--<div class="modal-inside">
                                <form action="<?php echo $action_image; ?>" method="post" enctype="multipart/form-data" id="form-photogallery">
                                    <input type="file" name="file">
                                    <input hidden type="text" name="id" value="<?=$image['id']; ?>">
                                    <button type="button" data-toggle="tooltip" title="<?php echo $button_change_image; ?>" class="change btn btn-success"><i class="fa fa-upload"></i></button>
                                </form>


                            </div> -->
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <label class="col-sm-2 control-label"
                                       for="input-text-<?php echo $k; ?>"><?php echo $entry_text; ?></label>
                                <div class="col-sm-8"><input type="text" name="images[<?php echo $k; ?>][text]"
                                                             value="<?=$image['text']; ?>"
                                                             placeholder="<?php echo $image['text']; ?>"
                                                             id="input-text-<?php echo $k; ?>"
                                                             class="form-control"/></div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 control-label"
                                       for="input-alt-<?php echo $k; ?>"><?php echo $entry_alt; ?></label>
                                <div class="col-sm-8"><input type="text" name="images[<?php echo $k; ?>][alt]"
                                                             value="<?=$image['alt']; ?>"
                                                             placeholder="<?php echo $image['text']; ?>"
                                                             id="input-alt-<?php echo $k; ?>"
                                                             class="form-control"/></div>
                                <input hidden type="text" name="images[<?php echo $k; ?>][id]"
                                       value="<?=$image['id']; ?>"  />
                            </div>
                            <div class="row">
                                <label class="col-sm-2 control-label"
                                       for="input-title-<?php echo $k; ?>"><?php echo $entry_title; ?></label>
                                <div class="col-sm-8"><input type="text" name="images[<?php echo $k; ?>][title]"
                                                             value="<?=$image['title']; ?>"
                                                             placeholder="<?php echo $image['title']; ?>"
                                                             id="input-title-<?php echo $k; ?>"
                                                             class="form-control"/></div>

                            </div>
                        </div>
                        <div class="col-sm-2">
                            <!-- $('#attribute-row<?php echo $attribute_row; ?>').remove();-->
                            <button type="button" onclick="removeThisPhoto(<?=$image['id']; ?>, <?=$k; ?>)"
                                    data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger">
                                <i class="fa fa-minus-circle"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <?php } ?>
                    <?php } ?>
                </div>
                </form>

                <div class="copyright text-right"><?php echo $coded_by; ?></div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        function removeThisPhoto(id, k) {
            console.log(id);

            $.ajax({
                url: 'index.php?route=module/photogallery/delete&token=<?php echo $token; ?>&id=' + id,
                dataType: 'json',
                success: function (json) {
                    if (json) {
                        console.log($(k));
                        $('#' + k).remove();
                    }
                }
            });
        }
        $('.form-group a').on('click', function() {
            return false;
        });
//        $('button.change').on('click', function() {
//            console.log(this);
//            var
//
//
//        });




        //    $(document).ready(function(){
        //      $('#images').on('change',function(){
        //        $('#form-category').ajaxForm({
        //          //display the uploaded images
        //          target:'#images_preview',
        //          beforeSubmit:function(e){
        //            $('.uploading').show();
        //          },
        //          success:function(e){
        //            $('.uploading').hide();
        //          },
        //          error:function(e){
        //          }
        //        }).submit();
        //      });
        //    });
    </script>
</div>
<?php echo $footer; ?>