<?php
class ModelExtensionPhotogallery extends Model {

	public function getImages($filter_data) {

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "photogallery` LIMIT ".$filter_data['start'] . ", " . $filter_data['limit']);
		return $query->rows;
	}

	public function getImagesCount() {
		$query = $this->db->query("SELECT COUNT(*) as count FROM `" . DB_PREFIX . "photogallery`");
		return $query->row['count'];
	}
}