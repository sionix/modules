<?php

class ControllerModulePhotogallery extends Controller
{
    //russian/module/photogallery
    //english/module/photogallery

    public function index()
    {
        $this->load->language('module/photogallery');
        $this->load->model('tool/image');
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_product_limit');
        }
        $data['heading_title'] = $this->language->get('heading_title');
        $this->load->model('extension/photogallery');

        $data['categories'] = array();
        $data['breadcrumbs'] = array();
        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        $this->document->setTitle($this->language->get('heading_title'));
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/photogallery')
        );
        $filter_data = array();
        $filter_data['limit'] = $limit;
        $filter_data['start'] = ($page - 1) * $limit;
        $photos = $this->model_extension_photogallery->getImages($filter_data);
        foreach ($photos as &$photo) {
            if ($photo['image']) {
                $photo['thumb'] = $this->model_tool_image->resize('photogallery/' . $photo['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                $photo['image'] = $this->config->get('config_ssl') . 'image/photogallery/' . $photo['image'];
            } else {
                $photo['thumb'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                $photo['image'] = $this->config->get('config_ssl') . 'image/placeholder.png';

            }
        }
        $count = $this->model_extension_photogallery->getImagesCount();
        $pagination = new Pagination();
        $pagination->total = $count;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('module/photogallery', 'page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($count) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($count - $limit)) ? $count : ((($page - 1) * $limit) + $limit), $count, ceil($count / $limit));


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['photos'] = $photos;
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/photogallery.tpl')) {
            return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/photogallery.tpl', $data));
        } else {
            return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/photogallery.tpl', $data));
        }
    }
}