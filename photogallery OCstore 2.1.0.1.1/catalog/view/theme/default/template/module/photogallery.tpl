<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <h1><?php echo $heading_title; ?></h1>
            <?php foreach ($photos as $photo) { ?>
            <div class="product-layout product-list col-xs-4">
                <div class="product-thumb thumbnails" style="text-align: center">
                    <h3><?=$photo['title']; ?></h3>
                    <a class="thumbnail" href="<?=$photo['image']; ?>"><img src="<?=$photo['thumb']; ?>"
                                                                            alt="<?=$photo['alt']; ?>"></a>
                    <p><?=$photo['text']; ?></p>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php echo $pagination; ?>
        <?php echo $results; ?>

    </div>
</div>
<script type="text/javascript"><!--
    $(document).ready(function () {
        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });
    //--></script>
<?php echo $footer; ?>