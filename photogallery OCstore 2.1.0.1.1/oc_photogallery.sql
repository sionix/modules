-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 20 2016 г., 10:09
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vipcrystal`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oc_photogallery`
--

CREATE TABLE IF NOT EXISTS `oc_photogallery` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `image` varchar(225) NOT NULL,
  `text` text NOT NULL,
  `alt` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `oc_photogallery`
--

INSERT INTO `oc_photogallery` (`id`, `image`, `text`, `alt`, `title`) VALUES
(1, '72941452849428.jpg', '1', 'fdsfds', 'ывафвыф'),
(2, '99511452849428.jpg', 'выфвыфв', 'выфвыф', 'выфвфы'),
(3, '20591452849428.jpg', 'выфвфы', 'выфвфы', 'выфв'),
(4, '21711452849428.jpg', 'вфавпа', 'рапрап', 'прп'),
(5, '24371452849428.jpg', 'рпарап', 'па', 'рпа'),
(6, '45461452849428.jpg', 'впв', 'рпврп', 'врвопрвор'),
(7, '48281452849428.jpg', 'опвпр', 'шнгшгн', 'гкнгнк'),
(8, '38661452849428.jpg', 'авыфаы', 'авф', 'раправ'),
(9, '64551452849428.jpg', 'рпарв', 'ывапвы', 'паывпаы');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
