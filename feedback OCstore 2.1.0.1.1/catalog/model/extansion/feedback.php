<?php
class ModelExtensionFeedback extends Model
{

    public function addFeedback($data)   {

        $query = $this->db->query("INSERT INTO `" . DB_PREFIX . "feedback` SET `published` = '0', `text` = '". $this->db->escape($data['text']) . "', `name` = '" . $this->db->escape($data['name']) ."', `email` = '". $this->db->escape($data['email']). "', `date` = CURDATE()");
        return $query;
    }
    public function getFeedbacks($data) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "feedback` WHERE `published` = '1' ORDER BY `date` DESC";
        if (isset($data['start'])) {
            $sql .= " LIMIT " . $this->db->escape($data['start']) . ", " . $data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }
    public function getTotalFeedbacks() {
        $sql = "SELECT count(*) as count FROM `" . DB_PREFIX . "feedback` WHERE `published` = '1' ORDER BY `date` DESC";
        $query = $this->db->query($sql);
        return $query->row['count'];
    }
}