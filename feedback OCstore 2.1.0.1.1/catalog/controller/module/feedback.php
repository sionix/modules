<?php

class ControllerModuleFeedback extends Controller
{
    private $pattern = '/^(\S+)@([a-z0-9-]+)(\.)([a-z]{2,4})(\.?)([a-z]{0,4})+$/';
    private $error;
    private $data;

    public function index()
    {
        $this->load->language('module/feedback');
        $data = array();
        $data['action'] = $this->url->link('module/feedback/add');
        $data['feedback_text'] = $this->language->get('feedback_text');
        $data['submit_text'] = $this->language->get('submit_text');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['success'] = $this->language->get('success');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_text'] = $this->language->get('entry_text');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/feedback_form.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/feedback_form.tpl', $data);
        } else {
            return $this->load->view('default/template/module/feedback_form.tpl', $data);
        }
    }

    public function add()
    {
        $json = array();
        $this->load->model('extension/feedback');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if ($this->model_extension_feedback->addFeedback($this->request->post)) {
                $json['success'] = true;

            }
        } else {
            $json['error'] = $this->error;
            $json['_data'] = $this->data;



        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));


    }

    protected function validateForm()
    {
        $this->load->language('module/feedback');

        if ((utf8_strlen($this->request->post['name']) < 3) || ($this->request->post['name']) > 50) {
            $this->error['name'] = $this->language->get('error_name');
        }
        $this->data['name'] = $this->request->post['name'];

        $this->data['email'] = $this->request->post['email'];

        if ((utf8_strlen($this->request->post['text']) < 20) || ($this->request->post['text'] > 255)) {
            $this->error['text'] = $this->language->get('error_text');

        }
        $this->data['text'] = $this->request->post['text'];
        // Captcha
//<?php echo $captcha;
//        if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
//            $captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');
//
//            if ($captcha) {
//                $json['error'] = $captcha;
//            }
//        }

        return !$this->error;
    }
}