<div class="feedback-wrapper"><?=$feedback_text; ?>
<div id="feedback">
    <form action="<?=$action; ?>" id="form_feedback" class="form-group">
        <div id="result_form"></div>
        <input type="text" name="name" value="" placeholder="<?php echo $entry_name; ?>" class="form-control" />
        <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" class="form-control" />
        <textarea rows="5" cols="79" placeholder="<?php echo $entry_text; ?>" class="form-control" name="text"></textarea>
        <div class="g-recaptcha" data-sitekey="6LcybyETAAAAANjUy1qc0JLZaAkrGf1wQp5d9517"></div>
        <button type="submit" form="form_feedback" class="pull-right" value="Submit"><?=$submit_text; ?></button>
        <div class="clearfix"></div>
    </form></div>
</div>

<style>

    .wrap {
        box-shadow: 0 0 10px rgba(200, 201, 201, 0.5);
        background: #fff;
        width: 100%;
        position: relative;
    }
    .wrap .block-text {
        padding: 25px;
    }
    .wrap .block-text h1 {
        color: #B7AA9C;
        font-size: 20px;
        margin: 30px 0;
    }
    .wrap .block-text hr {
        display: block;
        border: none;
        margin: 0;
        width: 30px;
        background: #efefef;
        margin-bottom: 25px;
        height: 2px;
    }
    .wrap .block-text p {
        font-weight: 300;
        line-height: 20px;
        font-size: 14px;
    }
    .wrap .block-date {
        position: relative;
        width: 100%;
        overflow: hidden;
    }
    .wrap .block-date-bg {
        position: absolute;
        bottom: 0;
        left: 0;
        overflow: hidden;
        width: 100%;
    }
    .wrap .date {
        color: #fff;
        position: relative;
        z-index: 1;
        font-weight: 300;
        letter-spacing: 1px;
        padding: 0px 30px 15px 30px;
        font-size: 18px;
        opacity: 0;
        -webkit-transition: all 0.25s;
        transition: all 0.25s;
        -webkit-transform: translateY(40px);
        transform: translateY(40px);
    }
    .wrap .day {
        float: left;
    }
    .wrap .time {
        float: right;
    }
    .wrap span {
        display: block;
        float: left;
        height: 10px;
        bottom: 0;
        left: 0;
        content: "";
    }
    .wrap span.dark {
        background: #367cb0;
    }
    .wrap span.light {
        background: #7BA6C5;
    }
    .alert.alert-danger {
        margin: 5px;
    }
    .wrap span.percent-75 {
        width: 75%;
    }
    .wrap span.percent-25 {
        width: 25%;
    }
    .feedback-wrapper input, .feedback-wrapper textarea {
        background-color: #fff !important;
        border-color: #e0e0e0 !important;
        -webkit-box-shadow: inset 0 1px 3px rgba(0,0,0,.06) !important;
        box-shadow: inset 0 1px 3px rgba(0,0,0,.06) !important;
        color: #474e57 !important;
        margin: 5px 0;
    }
    .feedback-wrapper input:focus, .feedback-wrapper textarea:focus {
        border-color: #adadad !important;
        -webkit-box-shadow: inset 0 1px 3px rgba(0,0,0,.06),0 0 5px rgba(160,160,160,.7) !important;
        box-shadow: inset 0 1px 3px rgba(0,0,0,.06),0 0 5px rgba(160,160,160,.7) !important;
        outline: 0;
    }
    .wrap {
        margin: 10px 0 0 0;
        box-shadow: 0 10px 20px rgba(200, 201, 201, 0.5);
    }
    .wrap .block-text h1 {
        margin-top: 5px;
    }
    .wrap .block-text hr {
        height: 1px;
    }
    .wrap .block-date .date {
        opacity: 1;
        padding: 25px 30px 15px 30px;
        -webkit-transform: translateY(0);
        transform: translateY(0);
    }
    .wrap .block-date span {
        height: 45px;
    }
</style>

<?php if (isset($feedbacks)) { ?>

    <?php foreach ($feedbacks as $feedback) { ?>
<div class="wrap">
    <div class="block-date">
        <p class="date time"><?= $feedback['date']; ?></p>
        <div class="block-date-bg"><span class="percent-75 dark"></span><span class="percent-25 light"></span></div>
    </div>
    <div class="block-text">
        <h1 class="title"><?= $feedback['name']; ?></h1>
        <hr/>
        <p class="text"><?= $feedback['text']; ?></p>
    </div>
</div>

<?php } ?>
<?php } ?>
<div class="pagination"><?=$pagination; ?></div>
<div class="pagination"><?=$results; ?></div>
<script>
    $('.feedback').on('click', function(e){
        e.preventDefault();
        $('#result_form').html('');
    });
    $("#form_feedback").submit(function(e) {

        var url = $(this).attr('action'); // the script where you handle the form input.
        $.ajax({
            type: "POST",
            url: url,
            data: $("#form_feedback").serialize(), // serializes the form's elements.

            success: function(data)
            {
                $('#result_form').empty();
                if (data.success == true) {
                   $('.feedback-wrapper').html(' <div class="alert alert-success"><?=$success; ?></div>');
                }
                if (data.error) {

                    $.each(data.error, function (index, value) {
                        $('#result_form').append(' <div class="alert alert-danger">' + value + '</div>')
                    });
                    $.each(data._data, function( index, value ) {
                        if (index == 'name') {
                            $('input[name=\'name\']').val(value)
                        } else if (index == 'email') {
                            $('input[name=\'email\']').val(value)
                        } else if (index == 'text') {
                            $('input[name=\'text\']').val(value)
                        }

                    });
                }
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

</script>
<!-- тут будет аякс проверка и отправка данных в контроллер -->