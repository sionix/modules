-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 20 2016 г., 10:11
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vipcrystal`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oc_feedback`
--

CREATE TABLE IF NOT EXISTS `oc_feedback` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `status` int(5) NOT NULL,
  `published` int(5) NOT NULL,
  `title_answer` text NOT NULL,
  `answer` text NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `oc_feedback`
--

INSERT INTO `oc_feedback` (`id`, `email`, `name`, `text`, `status`, `title_answer`, `answer`, `date`) VALUES
(23, 'admin@example', 'vasya', 'vse ok', 1, '', '', '2016-01-16'),
(24, 'admin@example', 'avasya', 'vse ok', 1, '', '', '2016-01-15'),
(25, 'emmail@example.com', 'Василий', 'Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения ', 1, '', '', '0000-00-00'),
(4, 'admin@example.com', 'Василий', 'dsafdshfdsjafjkdsafkdsak dsafdshfdsjafjkdsafkdsak\r\ndsafdshfdsjafjkdsafkdsak', 1, '', '', '0000-00-00'),
(5, 'admin@example.cpm', 'Василиса', 'текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст ', 1, '', '', '2016-01-18'),
(6, 'admin@example.cpm', 'Василиса', 'текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст текст ', 1, '', '', '2016-01-18'),
(7, 'авыаыфва ывфа', 'ававфавыф ', 'выфаывфа ывфа ыфа фыав ыфа выфа выф', 1, '', '', '2016-01-18'),
(8, 'авфавф', 'авфавыфаыф', 'аывфавыфавыфавыаывфаыфвафаыф', 1, 'Иди нахуй', 'АЗАЗА', '2016-01-18'),
(9, 'd', 'ffafd', 'affadfafdafdafdsaffadfafdafdafdsaffadfafdafdafdsaffadfafdafdafds', 1, '', '', '2016-01-18'),
(10, 'fadfasfdsaf', 'fdafdsafd', 'dsafddsafddsafddsafddsafddsafddsafddsafddsafddsafddsafddsafddsafd', 1, '', '', '2016-01-18'),
(11, 'fdafdsa', 'fdfdasfda', 'fdasfdafa faf sdaf saffdas fdsa', 1, '', '', '2016-01-18'),
(12, 'admin@example.com', 'Петя', ' fdfjdakjfsdkafk fdfjdakjfsdkafk fdfjdakjfsdkafk fdfjdakjfsdkafk ', 1, 'ваыфавыавфавфавыфавфыавфыаыфавфавф', 'авфыавыфавыфафвыавыфавыфавфавфавыф', '2016-01-18'),
(13, 'admin@example.com', 'Петя', ' fdfjdakjfsdkafk fdfjdakjfsdkafk fdfjdakjfsdkafk fdfjdakjfsdkafk ', 1, '', '', '2016-01-18'),
(14, 'fdsafdsa', 'fdsafdas', 'dgagdafdsafdsafdafdasfdsafdsafsa', 1, '', '', '2016-01-18'),
(15, 'afdafdasfas', 'gdfgdafdsafds', 'dgagdafdsafdsafdafdasfdsafdsafsa', 1, 'авфавфы', 'авфавыфавыфавы', '2016-01-18'),
(16, 'ghfgfdghdf', 'fdafdafda', 'fhfghfdghfgfdgfdgdfgfdgdfgdfg', 1, 'авфавфа', 'авфыавфыавыфавыф', '2016-01-18'),
(17, 'afdasfdsafdasf', 'gdfsgdfsfd', 'dafdafdadafdafdadafdafdadafdafdadafdafdadafdafdadafdafda', 1, '1', '1', '2016-01-18'),
(18, 'gfsdgfdsgf', 'gfsgfsgfsgfsd', 'sgfdsgfsdgfsdgfdsggfsgfsgfsgfs', 1, '1', '1', '2016-01-18'),
(19, 'fdfdafdafdafdad', 'fdfdafdafdafdad', 'fdfdafdafdafdadfdfdafdafdafdadfdfdafdafdafdadfdfdafdafdafdadfdfdafdafdafdad', 0, '', '', '2016-01-18'),
(20, 'admin@example.com', 'fdafdafda', 'fasfadfads fasfadfads fasfadfads fasfadfads fasfadfads fasfadfads fasfadfads ', 0, '', '', '2016-01-18'),
(21, 'fdasfdafdafdsaffdasfdafdafdsaf', 'fdasfdafdafdsaffdasfdafdafdsaf', 'fdasfdafdafdsaffdasfdafdafdsaffdasfdafdafdsaffdasfdafdafdsaffdasfdafdafdsaffdasfdafdafdsaffdasfdafdafdsaffdasfdafdafdsaffdasfdafdafdsaf', 1, '', '', '2016-01-18'),
(22, 'sionix1@ya.ru', 'Сергей', 'Здравствуйте, я бы хотел приобрести указанный бла бла бла товар', 1, 'title title', 'description description', '2016-01-18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
