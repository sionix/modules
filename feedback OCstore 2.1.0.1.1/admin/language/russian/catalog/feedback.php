<?php
// Heading
$_['heading_title']          		= 'Сообщения пользователей';

// Text
$_['text_success']          		= 'Список сообщений обновлен!';
$_['text_list']          			= 'Список сообщений пользователей';
$_['text_add']          			= 'Просмотр сообщения пользователя';
$_['text_edit']          			= 'Редактирование новости';
$_['text_default']          		= 'Основной магазин';

// Column
//$_['column_name']          			= 'Название новости';
$_['column_name']          		= 'Имя автора';
$_['column_status']          		= 'Прочитано/непрочитано';
$_['column_email']          		= 'E-mail автора';
$_['column_date']          		= 'Дата отзыва';
$_['column_sort_order']          	= 'Порядок сортировки';
$_['column_action']          		= 'Действие';

// Entry
$_['entry_title']          			= 'Название новости:';
$_['entry_description']          	= 'Описание';
$_['entry_meta_title']          	= 'HTML-тег Title';
$_['column_sort_date_modified']     = 'Дата последнего изменения';
$_['column_sort_date']       	= 'Дата написания';
$_['entry_meta_h1']          	    = 'HTML-тег H1';
$_['entry_meta_keyword']          	= 'Мета-тег Keywords';
$_['entry_meta_description']        = 'Мета-тег Description';
$_['entry_keyword']          		= 'SEO URL:';
$_['entry_name']          		= 'Имя';
$_['entry_answer']          		= 'Введите ответ который будет отправлен пользователю на указанный email';
$_['entry_title_answer']          		= 'Введите заголовок ответа который будет отправлен пользователю на указанный email';
$_['answer_placeholder']          		= 'Текст ответа';
$_['title_answer_placeholder']          		= 'Текст заголовка';
$_['entry_email']          		= 'Email';
$_['email_send']          		= 'Форма отправки email-ответа пользователю';
$_['entry_text']          		= 'Текст сообщения';
$_['entry_date']          		= 'Дата';
$_['entry_filter']          		= 'Фильтры:';
$_['entry_store']          			= 'Магазины:';
$_['entry_image']          			= 'Изображение новости';
$_['entry_top']          			= 'Главное меню:';
$_['entry_column']          		= 'Колонки:';
$_['button_read']          	     	= 'Прочесть/ответить';
$_['button_readed']          	   	= 'Отметить прочитанными';
$_['entry_sort_order']          	= 'Порядок сортировки:';
$_['entry_status']          		= 'Статус:';
$_['entry_layout']          		= 'Выберите макет:';
$_['status_readed']          		= 'Прочитано';
$_['status_not_readed']          		= 'Не прочитано';

// Help
$_['help_filter']          			= '(Автодополнение)';
$_['help_keyword']          		= 'Замените пробелы на тире. Должно быть уникальным на всю систему.';
$_['help_top']          			= 'Показывать в верхнем меню (работает только для основных новостей)';
$_['help_column']          			= 'Число столбцов для показа категорий 3-го уровня (работает только для основных новостей)';

// Error
$_['error_keyword']          		= 'Этот SEO keyword уже используется!';
$_['error_meta_title']          	= 'Мета-тег Title должен быть от 3 до 255 символов!';
$_['error_name']          			= 'Название категории должно быть от 2 до 32 символов!';
$_['error_permission']          	= 'У Вас нет прав для изменения категорий!';
$_['error_warning']          		= 'Внимательно проверьте форму на ошибки!';