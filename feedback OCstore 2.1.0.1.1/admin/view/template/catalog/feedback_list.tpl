<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-feedback"  title="<?php echo $button_readed; ?>" value="Submit" class="btn btn-success"><i class="fa fa-check-circle"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $mark_readed; ?>" method="post" enctype="multipart/form-data" id="form-feedback">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
              <tr>
                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>

                <td class="text-left"><?php if ($sort == 'name') { ?>
                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php if ($sort == 'date') { ?>
                  <a href="<?php echo $sort_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_date; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_date; ?>"><?php echo $column_sort_date_added; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php if ($sort == 'email') { ?>
                  <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php if ($sort == 'status') { ?>
                  <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php echo $column_action; ?></td>
              </tr>
              </thead>
              <tbody>
              <?php if ($feedbacks) { ?>
              <?php foreach ($feedbacks as $feedback) { ?>
              <tr>
                <td class="text-center"><?php if (in_array($feedback['id'], $selected)) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $feedback['id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $feedback['id']; ?>" />
                  <?php } ?></td>
                <td class="text-left"><?php echo $feedback['name']; ?></td>
                <td class="text-right"><?php echo $feedback['date']; ?></td>
                <td class="text-right"><?php echo $feedback['email']; ?></td>
                <td class="text-right"><?php echo $feedback['status']; ?></td>
                <td class="text-right"><a href="<?php echo $feedback['read']; ?>" data-toggle="tooltip" title="<?php echo $button_read; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("#form-feedback").submit(function(e) {

    var url = $(this).attr('action'); // the script where you handle the form input.

    $.ajax({
      type: "POST",
      url: url,
      data: $("#form-feedback").serialize(), // serializes the form's elements.
      success: function(data)
      {
        window.location.reload(true);
      }
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.
  });
</script>
<?php echo $footer; ?>