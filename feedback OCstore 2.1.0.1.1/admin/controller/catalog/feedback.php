<?php

class ControllerCatalogFeedback extends Controller
{
    private $error = array();
    //admin
//language/catalog/feedback
//model/catalog/feedback
//view/catalog/feedback_list

//catalog
//view/module/feedback
    public function index()
    {
        $this->language->load('catalog/feedback');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('catalog/feedback');
        $this->getList();
    }


    public function delete()
    {
        $this->language->load('catalog/news');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/news');
        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $news_id) {
                $this->model_catalog_news->deleteNews($news_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/news', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList()
    {

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['mark'] = $this->url->link('catalog/feedback/mark', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('catalog/feedback/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['feedbacks'] = array();
        $data['mark_readed'] = $this->url->link('catalog/feedback/readed', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['button_readed'] = $this->language->get('button_readed');
        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );
        $feedback_total = $this->model_catalog_feedback->getTotalFeedbacks();
        $results = $this->model_catalog_feedback->getFeedbacks($filter_data);
        foreach ($results as $result) {
            $data['feedbacks'][] = array(
                'id' => $result['id'],
                'name' => $result['name'],
                'date' => $result['date'],
                'email' => $result['email'],
                'status' => $result['status'],
                'read' => $this->url->link('catalog/feedback/read', 'token=' . $this->session->data['token'] . '&feedback_id=' . $result['id'] . $url, 'SSL')//видимо пока заглушка под отправку ответа
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');


        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_email'] = $this->language->get('column_email');
        $data['column_date'] = $this->language->get('column_date');

        $data['column_sort_date'] = $this->language->get('column_sort_date');
        $data['column_sort_date_added'] = $this->language->get('column_sort_date');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_read'] = $this->language->get('button_read');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $data['sort_status'] = $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
        $data['sort_date'] = $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . '&sort=date' . $url, 'SSL');
        $data['sort_email'] = $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
        //	$data['sort_sort_date_modified'] = $this->url->link('catalog/news', 'token=' . $this->session->data['token'] . '&sort=n.date_modified' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $feedback_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($feedback_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($feedback_total - $this->config->get('config_limit_admin'))) ? $feedback_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $feedback_total, ceil($feedback_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/feedback_list.tpl', $data));
    }


    public function readed()
    { //пометить прочитанными
        $this->load->model('catalog/feedback');
        foreach ($this->request->post['selected'] as $id) {
            $this->model_catalog_feedback->markReaded($id);

        }


    }

    public function save()
    { //пометить прочитанными
        $this->load->model('catalog/feedback');
        $this->language->load('mail/feedback_answer');
        $this->model_catalog_feedback->updateStatus($this->request->post);
        if (isset($this->request->post['answer']) && isset($this->request->post['title_answer'])) {
            $data = array();
            $data['text_greeting'] = sprintf($this->language->get('text_new_greeting'), $this->request->post['name']);
            $data['store_name'] = $this->config->get('config_meta_title');
            $data['store_url'] = HTTP_CATALOG;
            $data['logo'] = $this->config->get('config_logo');
            $data['title_answer'] = $this->request->post['title_answer'];
            $data['answer'] = $this->request->post['answer'];

            $html = $this->load->view('/mail/feedback_answer.html', $data);
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->request->post['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($this->config->get('config_meta_title'), ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($this->request->post['title_answer'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($html);
           // $mail->setText($text);
            $mail->send();
            $this->model_catalog_feedback->saveEmail($this->request->post);
            $d = array();
            $d['id'] = $this->request->post['id'];

            $d['status'] = 1;
            $this->model_catalog_feedback->updateStatus($d);


        }
        $this->response->redirect($this->url->link('catalog/feedback', 'token=' . $this->session->data['token'], 'SSL'));

    }

    public function read()
    { //читать отзыв (возможно писать ответ на email - не подтверждено пока ещё в тз)
        $this->language->load('catalog/feedback');
        $this->load->model('catalog/feedback');

        if ($this->config->get('config_editor_default')) {
            $this->document->addScript('view/javascript/ckeditor/ckeditor.js');
            $this->document->addScript('view/javascript/ckeditor/ckeditor_init.js');
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_form'] = !isset($this->request->get['news_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_readed'] = $this->language->get('status_readed');
        $data['text_not_readed'] = $this->language->get('status_not_readed');

        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_title_answer'] = $this->language->get('entry_title_answer');
        $data['title_answer_placeholder'] = $this->language->get('title_answer_placeholder');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_bottom'] = $this->language->get('entry_bottom');
        $data['email_send'] = $this->language->get('email_send');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_layout'] = $this->language->get('entry_layout');

        $data['help_keyword'] = $this->language->get('help_keyword');
        $data['help_bottom'] = $this->language->get('help_bottom');

        $data['button_save'] = $this->language->get('button_save');


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $feedback = $this->model_catalog_feedback->getFeedback($this->request->get['feedback_id']);
        $data['name'] = $feedback['name'];
        $data['email'] = $feedback['email'];
        $data['text'] = $feedback['text'];
        $data['date'] = $feedback['date'];
        $data['readed'] = $feedback['status'];
        $data['title_answer'] = $feedback['title_answer'];
        $data['answer'] = $feedback['answer'];
        $data['answer_placeholder'] = $this->language->get('answer_placeholder');
        $data['id'] = $feedback['id'];
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_answer'] = $this->language->get('entry_answer');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_text'] = $this->language->get('entry_text');
        $data['entry_date'] = $this->language->get('entry_date');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['error_warning'] = '';


        $data['action'] = $this->url->link('catalog/feedback/save', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('catalog/feedback', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->get['news_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $news_info = $this->model_catalog_news->getNewsItem($this->request->get['news_id']);
        }

        $data['token'] = $this->session->data['token'];
        $data['ckeditor'] = $this->config->get('config_editor_default');



        if (isset($this->request->post['keyword'])) {
            $data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($news_info)) {
            $data['keyword'] = $news_info['keyword'];
        } else {
            $data['keyword'] = '';
        }


        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($news_info)) {
            $data['status'] = $news_info['status'];
        } else {
            $data['status'] = true;
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/feedback_form.html', $data));

    }
}