<?php
class ModelCatalogFeedback extends Model {


	public function markReaded($id) {
		$this->db->query("UPDATE " . DB_PREFIX . "feedback SET status = '1' WHERE id = '" . (int)$id . "'");

	}




	public function getFeedbacks($filter_data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "feedback";
			if ($filter_data['sort']) {
				$sql .= " ORDER BY ".$filter_data['sort'] ;
			}
			if ($filter_data['order']) {
				$sql .= " ".$filter_data['order'] ;
			}
			if (isset($filter_data['start'])) {
				$sql .= " LIMIT ".$filter_data['start'] . ", " .$filter_data['limit'];
			}

			$query = $this->db->query($sql);
			return $query->rows;

	}

	public function getFeedback($id) {

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "feedback WHERE `id` = '" . (int)$id . "'");


		return $query->row;
	}
	public function updateStatus($data) {

		$this->db->query("UPDATE " . DB_PREFIX . "feedback SET status = '" . (int)$data['status'] . "' WHERE id = '" .  (int)$data['id']. "'");


	}
	public function saveEmail($data) {
		$this->db->query("UPDATE " . DB_PREFIX . "feedback SET `title_answer` = '" . $data['title_answer'] . "', `answer` = '" . $data['answer'] . "' WHERE id = '" .  (int)$data['id']. "'");
	}


	public function getTotalFeedbacks() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "feedback");

		return $query->row['total'];
	}

}